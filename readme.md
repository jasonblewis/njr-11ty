my 11ty version of notjustrecipes.com
2023-05-18 - JBL

# to sync to remote site

time rsync -avz -e ssh _site/ jason@nyanza:/var/www/html/notjustrecipes.com

# WordPress Clone
this is an attempt to make a static html copy of a wordpress site. I cloned the original site using httrack, then moved it into 11ty

# programatically rewriting links
consider using perl Mojo::DOM to edit html tags in files
